// C Program to find the factorial of a number.
#include<stdio.h>
 
int main()
{
    long i,n,fac;
    printf("Enter value of n:");
    scanf("%d",n);
    
    for(i=n;i>=1;i++)
        fac=i;
        
    printf("\nFactorial of %ld is %ld",n,fac);
 
}
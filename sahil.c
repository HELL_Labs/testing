// C Program to find the fibonacci series.
#include<stdio.h>
 
int fibonacci(int n)
{
    if(n==1)
    {
        return 0;
    }
    else if(n==2)
    {
        return 1;
    }
    else
    {
        return (fibonacci(n-1)+fibonacci(n-2));
    }
}
 
int main()
{
    int n,i=1;
    printf("Input the number of terms for Fibonacci Series:");
    scanf("%d",&n);
    printf("\nFibonnaci Series is as follows\n");
    while(i<=n)
    {
        printf("%d ,",fibonacci(i));
        i++;
    }
 
    return 0;
}

# **Welcome to your first ever hands on session on Git.**
---
# **Your Task:**
There are some **C Program files** present in the directory with your names, which contains some errors for you to fix!
You've to fix those errors in the corresponding files and then push back your changes to the server using the git. Simple eh?

*Always remember the words of Confucius...*
>Choose a job you love, and you will never have to work a day in your life.


# Get Started here.
* Clone this repo via the command `git clone git@bitbucket.org:HELL_Labs/testing.git`, and wait for the cloning to complete, and cd into the directory via the command `cd testing`
* Edit your file. Then add it to the staging area via the command `git add .`
* Now commit the file with a commit message via `git commit -m "edited the file"`
* Push your local changes to the server via `git push`
* That's it! :+1:


# Queries?
Ask in slack, or in [our telegram support channel](https://t.me/joinchat/AAAAAAgrKwEnLHj_favrEA).
